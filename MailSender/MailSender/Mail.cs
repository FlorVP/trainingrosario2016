﻿namespace MailSender
{
    public class Mail
    {
        public string To { get; set; }

        public string Bcc { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }
}