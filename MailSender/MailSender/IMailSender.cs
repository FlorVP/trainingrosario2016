﻿namespace MailSender
{
    public interface IMailSender
    {
        void SendMail(Mail mail);
    }
}
